package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WeatherResponsesDTO {

    @SerializedName("cod")
    private String cod;

    @SerializedName("calctime")
    private float calctime;

    @SerializedName("cnt")
    private int cnt;

    @SerializedName("list")
    @Expose // WHY???
    ArrayList<WeatherResponseDTO> list;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public float getCalctime() {
        return calctime;
    }

    public void setCalctime(float calctime) {
        this.calctime = calctime;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public ArrayList<WeatherResponseDTO> getList() {
        return list;
    }

    public void setList(ArrayList<WeatherResponseDTO> list) {
        this.list = list;
    }
}
