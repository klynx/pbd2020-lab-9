package si.uni_lj.fri.pbd.lab9.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import si.uni_lj.fri.pbd.lab9.models.dto.WeatherResponsesDTO;

public interface RestAPI {
    @GET("data/2.5/box/city?")
    Call<WeatherResponsesDTO> getCurrentWeatherData(@Query("bbox") String bbox, @Query("appid") String app_id);
}
