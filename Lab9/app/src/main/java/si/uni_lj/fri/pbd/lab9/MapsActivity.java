package si.uni_lj.fri.pbd.lab9;

import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.lab9.models.dto.WeatherResponseDTO;
import si.uni_lj.fri.pbd.lab9.models.dto.WeatherResponsesDTO;
import si.uni_lj.fri.pbd.lab9.rest.RestAPI;
import si.uni_lj.fri.pbd.lab9.rest.ServiceGenerator;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap mMap;
    private RestAPI mRestClient;
    private ConnectivityManager mNwManager;
    private ConnectivityManager.NetworkCallback mNwCallback;
    private boolean mOnUnmetered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Start RestAPI service
        mRestClient = ServiceGenerator.createService(RestAPI.class);
        mOnUnmetered = false;
        mNwCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onCapabilitiesChanged(Network network, NetworkCapabilities nkc) {
                mOnUnmetered = nkc.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_METERED);
                Log.d(TAG, "Unmetered: " + mOnUnmetered);
            }
        };

        mNwManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mNwManager.registerDefaultNetworkCallback(mNwCallback);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mNwManager.unregisterNetworkCallback(mNwCallback);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        // set onCameraIdleListener
        mMap.setOnCameraIdleListener(this);

        // set starting location to Ljubljana
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(46, 14.5), 8.0f) );
    }

    @Override
    public void onCameraIdle() {
        if (!mOnUnmetered) {
            Toast.makeText(getApplicationContext(), R.string.unmetered_text, Toast.LENGTH_LONG).show();
            return;
        }

        Log.d(TAG, "onCameraIdle");
        String zoom = "10";
        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

        String lon_left = Double.toString(bounds.southwest.longitude);
        String lat_bottom = Double.toString(bounds.southwest.latitude);;
        String lon_right = Double.toString(bounds.northeast.longitude);
        String lat_top = Double.toString(bounds.northeast.latitude);;

        String bbox = Stream.of(lon_left, lat_bottom, lon_right, lat_top, zoom)
                .collect(Collectors.joining(","));

        Log.d(TAG, "BBOX STRING: " + bbox);

        mRestClient.getCurrentWeatherData(bbox, Constants.API_KEY)
                .enqueue(new Callback<WeatherResponsesDTO>() {
                    @Override
                    public void onResponse(Call<WeatherResponsesDTO> call, Response<WeatherResponsesDTO> response) {
                        Log.d(TAG, "onResponse");
                        if (response.isSuccessful()) {
                            Log.d(TAG, "response was successful");
                            ArrayList<WeatherResponseDTO> weathers = response.body().getList();
                            weathers.stream()
                                    .forEach(what -> {
                                        mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(what.getCoord().getLat(), what.getCoord().getLon()))
                                            .title(what.getName() + " " + what.getMain().getTemp())
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                                    });
                        } else {
                            Log.d(TAG, "response was not successful");
                            Log.d(TAG, response.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<WeatherResponsesDTO> call, Throwable t) {
                        Log.d(TAG, "onFailure");
                        Log.d(TAG, call.request().toString());
                        Log.d(TAG, t.toString());
                    }
                });

    }

}
