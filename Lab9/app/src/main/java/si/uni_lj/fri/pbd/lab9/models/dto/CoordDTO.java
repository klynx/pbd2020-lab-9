package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.SerializedName;

public class CoordDTO {
    @SerializedName("Lon")
    private float Lon;

    @SerializedName("Lat")
    private float Lat;

    public float getLon() {
        return Lon;
    }

    public void setLon(float lon) {
        Lon = lon;
    }

    public float getLat() {
        return Lat;
    }

    public void setLat(float lat) {
        Lat = lat;
    }
}
